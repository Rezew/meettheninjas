const express = require('express');
const path = require('path');
const fetch = require('node-fetch');
const GroupBy = require('./functions/GroupBy');
const SortBy = require('./functions/SortBy');
const app = express();
app.set( 'port', ( process.env.PORT || 8080 ));

app.use(express.static(path.join(__dirname, '/build')));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'))
});

app.get('/api/ninjas', async (req, res) => {
  const response = await fetch('https://api.tretton37.com/ninjas');
  const data = await response.json();
  try {
    let grouped = await GroupBy(data, 'office');
    for (let [key, value] of Object.entries(grouped))
    {
      grouped[key] = value.sort(SortBy('name'));
    }

    res.send(grouped)
  } catch (e) {
    res.error()
  }
});

app.listen(app.get( 'port' ), () => console.log(`Server listening on port: ${app.get( 'port' )}`));