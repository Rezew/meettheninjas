import React from "react";
import "./ninjaCard.css"
import {ReactComponent as GitHubIcon} from "./Github.svg";
import {ReactComponent as StackOverflowIcon} from "./stackOverflow.svg";
import {ReactComponent as TwitterIcon} from "./twitter.svg";
import {ReactComponent as LinkedInIcon} from "./linkedIn.svg";

function NinjaCard({ninja}) {
  const githubURL = 'https://github.com/';
  const linkedInURL = 'https://www.linkedin.com';
  const twitterURL = 'https://twitter.com/';
  const stackOverflowURL = 'https://stackoverflow.com/users/';

  return (
    <div className="ninja">
      <div className="image">
        <img src={ninja.imagePortraitUrl} alt={ninja.imagePortraitUrl} />
      </div>
      <div className="card-content">
        <div>
          <p>{ninja.name}</p>
          <p>Office: <span>{ninja.office}</span></p>
        </div>
        <div className="social-links">
          {ninja.stackOverflow && <a href={stackOverflowURL + ninja.stackOverflow} target="_blank" rel="noopener noreferrer"><StackOverflowIcon className="icon" /></a>}
          {ninja.gitHub && <a href={githubURL + ninja.gitHub} target="_blank" rel="noopener noreferrer"><GitHubIcon className="icon" /></a>}
          {ninja.linkedIn && <a href={linkedInURL + ninja.linkedIn} target="_blank" rel="noopener noreferrer"><LinkedInIcon className="icon" /></a>}
          {ninja.twitter && <a href={twitterURL + ninja.twitter} target="_blank" rel="noopener noreferrer"><TwitterIcon className="icon" /></a>}
        </div>
        <button className="ninja-read">READ MORE</button>
      </div>
    </div>
  )
}

export default NinjaCard;