import React, {useEffect, useState} from 'react';
import NinjaCard from "./components/NinjaCard";

import "./app.css";

function App() {
  const [office, settOffice] = useState({});
  const [err, setErr] = useState(false);

  const getNinjas = () => fetch('/api/ninjas')
    .then(res => res.json());

  useEffect(() => {
    getNinjas()
      .then(settOffice)
      .catch(() => setErr(true));
  }, []);

  const renderByOffice = () => {
    if (Object.entries(office).length) {
      return Object.entries(office).map(([key, value]) => {
        return (<React.Fragment key={key}>
          <div className="office-title">
            <p>Office: {key}</p>
          </div>
          {value.map(ninja => <NinjaCard key={ninja.email} ninja={ninja}/>)}
        </React.Fragment>);
      });
    }
  };

  return (
    <div className="App">
      <section className="container">
        <div className="ninja-list">
          {err && 'something went wrong'}
          {renderByOffice()}
        </div>
      </section>
    </div>
  );
}

export default App;
