module.exports = function(items, key) {
  return items.reduce(
    (result, item) => {
      let newKey = item[key].replace(/å/g, 'a')
        .replace(/Å/g, 'A')
        .replace(/ä/g, 'a')
        .replace(/Ä/g, 'A')
        .replace(/ö/g, 'o')
        .replace(/Ö/g, 'O');
      return ({
      ...result,
      [newKey]: [
        ...(result[newKey] || []),
        item,
      ],
    })}, {},
  );
};