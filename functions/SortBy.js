module.exports = function SortBy (propName) {
  return (a, b) => {
    const propertyA = a[propName].toLowerCase();
    const propertyB = b[propName].toLowerCase();
    if (propertyA === propertyB){
      return 0;
    } else if (propertyA < propertyB ) {
      return -1;
    } else if (propertyA > propertyB) {
      return 1;
    }
  }
};