# Meet the Ninjas

Simple meet page for _tretton37 built with express and react and ordinary CSS.

## Getting Started

To get started with development clone the repo

`git clone https://gitlab.com/Rezew/meettheninjas.git`

and the you have to install the dependencies for the app

`npm run install`

to start the dev servers for react and express run following commands.

`npm run start:react`

`npm run start:dev:server`

## Running the tests

Tests are not yet implemented!

## Deployment

To deploy from gitlab to an heroku server you have to setup the following environment variables in gitlab CI/CD
```
HEROKU_API_KEY = <your_heroku_api_key>
HEROKU_APP_PRODUCTION = <your_herku_app_name>
```
then all you have to do is push to master. ;)
you can change which branch that deploys in `.gitlab-ci.yml` file

## Built With

* [Create-React-App](https://github.com/facebook/create-react-app) - Tool for building React apps
* [ExpressJS](https://expressjs.com/) - web framework for Node.js
* [NodeJS](https://nodejs.org/en/) - JavaScript runtime

## Authors

* **Joakim Hall** - *Initial work* - [My GitLab](https://gitlab.com/Rezew)
